import sys
import cmd_parser
import texture_map_generator

def main(argv):
	arg_parser = cmd_parser.CmdParser(argv)

	gen = texture_map_generator.TextureMapGenerator(arg_parser.get_arg("-f"), arg_parser.get_arg("-o"), int(arg_parser.get_arg("-w")))
	gen.generate_textures()

if __name__ == "__main__":
	if len(sys.argv) < 2:
		print """gpl2texturemap.py:
		-f \t palette file 
		-o \t output file (ex: 'palette1.png;palette2.png;palette3.png')
		-w \t output width"""
	else:
		main(sys.argv)

	#main()
