import itertools, collections

class CmdParser:

	params = {}

	def __init__(self, argv):

		for i in range(0, len(argv)):
			arg = argv[i]

			# not an argument
			if arg[0] != '-':
				continue

			self.params[arg] = argv[i+1]
			print arg, self.params[arg]

	def get_arg(self, arg):
		return self.params[arg]


