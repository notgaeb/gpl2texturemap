import sys
from PIL import Image

class TextureMapGenerator:

	palette_file = ''
	output_files = ''
	width = 0

	def __init__(self, palette_file, output_file = 'palette_png', width = 8):
		self.palette_file = palette_file
		self.output_files = output_file.split(';')
		self.width = width

	def line_to_color(self, line):
		channels = []

		channel_str = ''
		for c in line:
			if c == ' ' and len(channel_str) > 0:
				channels.append(channel_str)
				channel_str = ''
				continue

			if c != ' ':
				channel_str = channel_str + c

		if len(channel_str) > 0:
			channels.append(channel_str)

		return channels

	def read_file(self):
		"""
		Reads a .gpl file and returns an array of color strings without their titles.
		"""
		f = open(self.palette_file, 'r')
		lines = f.readlines()

		if lines[0].strip() != 'GIMP Palette':
			print 'Not a GIMP Palette file. (.gpl)'
			sys.exit()

		# remove header
		lines = lines[2:]

		# remove titles
		for i in range(len(lines)):
			line = lines[i]
			line = line[:line.index('\t')]
			lines[i] = line

		return lines

	def generate_textures(self):
		# read palette_file
		colors = self.read_file()

		w = self.width
		h = (len(colors) / w) / len(self.output_files)
		colors_per_file = w * h;

		i = 0
		for output in self.output_files:

			img = Image.new('RGB', (w, h))
			pxls = img.load()
			print img.size[0], img.size[1]

			for y in range(h):
				for x in range(w):
					
					channels = self.line_to_color(colors[i])

					pxls[x, y] = (int(channels[0].strip()), int(channels[1].strip()), int(channels[2].strip()))
					i = i+1

			
			img.save(output)
			
		print 'Palettes generated with success.'



		
